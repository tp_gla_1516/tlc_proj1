package main

import (
	"html/template"
	"net/http"

	"appengine"
	"appengine/datastore"
	"strconv"
	"math/rand"
	"time"
	"log"
)

var templates = template.Must(template.ParseGlob("template/*"))
var listTitle = []string{"Bonjour","Salut","Yop","Plop","Hello"}
var listPrice = []int{1,3,56,34,42}
var listDate = []string{"2016-01-01","2014-12-01","2346-10-21","2204-04-05","2016-02-20"}



type Advert struct {
	Title  	string
	Price 	int
	Date    time.Time
}

type AdvertModelV struct {
	Id 		int64
	Title  	string
	Price 	int
	Date    string
}

type DataView struct {
	Models 			[]AdvertModelV
	DataSearchTitre 	string
	DataSearchDate 		string
	DataSearchDateBegin	string
	DataSearchDateEnd	string
	DataSearchPrice 	string
	DataSearchPriceBegin	string
	DataSearchPriceEnd	string
}

func init() {
	http.HandleFunc("/", root)
	http.HandleFunc("/addAd", addAd)
	http.HandleFunc("/addMultipleAd", addMutipleAd)
	http.HandleFunc("/clearAllAd", clearAllAd)
	http.HandleFunc("/clearListAd", clearListAd)
	http.HandleFunc("/removeAd", removeAd)
	http.HandleFunc("/searchAd", filterAd)

}

func root(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	q := datastore.NewQuery("Advert").Ancestor(adBoardKey(c)).Limit(10)

	adverts := getAdvertToShow(w, q, c)
	data := DataView{
		Models : adverts,
		DataSearchTitre : "",
		DataSearchDate : "",
		DataSearchDateBegin : "",
		DataSearchDateEnd : "",
		DataSearchPrice : "",
		DataSearchPriceBegin : "",
		DataSearchPriceEnd : "",
	}

	err := templates.ExecuteTemplate(w, "indexPage", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func getAdvertToShow(w http.ResponseWriter,q *datastore.Query, c appengine.Context) []AdvertModelV{
	adverts := make([]Advert, 0, 10)
	keys, err := q.GetAll(c, &adverts);
	if  err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}

	advertsToShow := make([]AdvertModelV, len(adverts))
	for i := 0; i < len(adverts); i++ {
		advertsToShow[i].Id = keys[i].IntID()
		advertsToShow[i].Title = adverts[i].Title
		advertsToShow[i].Date = adverts[i].Date.Format("02/01/2006")
		advertsToShow[i].Price = adverts[i].Price
	}

	return advertsToShow
}
func adBoardKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Advert", "advert", 0, nil)
}

func addMutipleAd (w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	nbAd, err := strconv.Atoi(r.FormValue("nbAd"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for i:=0; i<nbAd; i++ {
		err := addAux(c,listTitle[rand.Intn(5)],parseDate(listDate[rand.Intn(5)]), listPrice[rand.Intn(5)])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
func addAd(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	p,_ := strconv.Atoi(r.FormValue("inputPrix"))
	err := addAux(c,r.FormValue("inputTitre"),parseDate(r.FormValue("inputDate")),p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
func addAux(c appengine.Context,title string,date time.Time,price int) error{
	a := Advert{ Title: title, Date: date, Price :price, }

	key := datastore.NewIncompleteKey(c, "Advert", adBoardKey(c))
	_, err := datastore.Put(c, key, &a)
	return err
}

func clearAllAd(w http.ResponseWriter, r *http.Request) {

	c := appengine.NewContext(r)
	keys , err := datastore.NewQuery("Advert").KeysOnly().GetAll(c,nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	datastore.DeleteMulti(c,keys)
	http.Redirect(w, r, "/", http.StatusFound)
}
func clearListAd(w http.ResponseWriter, r *http.Request) {
	/*c := appengine.NewContext(r)
	keys , err := datastore.NewQuery("Advert").KeysOnly().GetAll(c,nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	datastore.DeleteMulti(c,keys)
	http.Redirect(w, r, "/", http.StatusFound)*/
}
func removeAd(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	id := r.FormValue("id")
	n ,errConvert := strconv.Atoi(id)
	if errConvert != nil {
		http.Error(w, errConvert.Error(), http.StatusInternalServerError)
		return
	}
	q := datastore.NewQuery("Advert")
	as := q.Run(c);
	for {
		var a Advert
		k, err := as.Next(&a)
		if err == datastore.Done {
			break
		}
		if k.IntID() == int64(n) {
			datastore.Delete(c,k)
		}
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

func filterAd(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	t := r.FormValue("inputSearchTitre")

	q := datastore.NewQuery("Advert")
	if t != ""  {
		q = q.Filter("Title =",t)
	}
	q = filterAdPriceAux(r,q)
	q = filterAdDateAux(r,q)

	adverts :=  getAdvertToShow(w, q.Ancestor(adBoardKey(c)).Limit(10), c)
	data := DataView{
		Models : adverts,
		DataSearchTitre : t,
		DataSearchDate : "",
		DataSearchPrice : "",
		DataSearchDateBegin : "",
		DataSearchDateEnd : "",
		DataSearchPriceBegin : "",
		DataSearchPriceEnd : "",
	}
	err := templates.ExecuteTemplate(w, "indexPage", data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func filterAdPriceAux(r *http.Request, q *datastore.Query) *datastore.Query {
	radio := r.FormValue("optionFiltrePrix")
	if radio != "interval" && radio != ""{
		pS := r.FormValue("inputSearchPrix")
		if pS != ""  {
			p,_ := strconv.Atoi(pS)
			op := "="
			if radio == "plus"{
				op = ">"
			}
			if radio == "moins"{
				op = "<"
			}
			q = q.Filter("Price " + op,p)
		}
	}
	if radio == "interval"{
		prixHaut := r.FormValue("inputSearchPrixEnd")
		if prixHaut != ""  {
			q = q.Filter("Price <=",prixHaut)
		}
		prixBas := r.FormValue("inputSearchPrixBegin")
		if prixBas != ""  {
			q = q.Filter("Price >=",prixBas)
		}
	}
	return q
}

func filterAdDateAux(r *http.Request, q *datastore.Query) *datastore.Query {
	radio := r.FormValue("optionFiltreDate")
	if radio != "interval" && radio != ""{
		d := r.FormValue("inputSearchDate")
		log.Print(d)
		if d != ""  {

			op := "="
			if radio == "apres"{
				op = ">"
			}
			if radio == "avant"{
				op = "<"
			}
			q = q.Filter("Date " + op,parseDate(d))
		}
	}
	if radio == "interval"{
		dateFin := r.FormValue("inputSearchDateEnd")
		if dateFin != ""  {
			q = q.Filter("Date <=",parseDate(dateFin))
		}
		dateDebut := r.FormValue("inputSearchDateBegin")
		if dateDebut != ""  {
			q = q.Filter("Date >=",parseDate(dateDebut))
		}
	}
	return q
}

func parseDate(date string) time.Time {
	d, _ := time.Parse("2006-01-02", date)
	return d
}

